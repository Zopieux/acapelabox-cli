#!/usr/bin/env python3

from distutils.core import setup

setup(
    name='acapelabox-cli',
    version='0.1',
    description='A command-line AcapelaBox TTS generator',
    author='Alexandre `Zopieux` Macabies',
    author_email='web@zopieux.com',
    url='https://bitbucket.org/zopieux/acapelabox-cli',
    scripts=['acapelabox'],
)
