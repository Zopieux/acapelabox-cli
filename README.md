# acapelabox-cli

A tiny command-line Python 3 script to retrieve text-to-speech sound files from [Apacala Box](https://www.acapela-box.com/), demo version.

## Usage

You can list the available voices with

```shell
acapelabox voices [optional search query eg. "english"]
```

You generate a sentence with the `speak` command:

```shell
acapelabox speak ryan22k 'Hello world!' | mpg123 -q -
```

## Disclaimer

Please use this software responsibly, as the Acapela Box demo is provided free-of-charges. There is a limitation over the length of the input sentence.

Acapela Box is a product of ©*Acapela Group*. This software is not related in any way with *Acapela Group*.